import "./App.css";
import DeleteMethod from "./components/DeleteMethod";
import GetMethod from "./components/GetMethod";
import PostMethod from "./components/PostMethod";
import PutMethod from "./components/PutMethod";

function App() {
  return (
    <div className="App">
      <GetMethod />
      <PostMethod />
      <PutMethod />
      <DeleteMethod />
    </div>
  );
}

export default App;
