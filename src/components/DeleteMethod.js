import React, { Component } from "react";
import axios from "axios";

class DeleteMethod extends Component {
  constructor(props) {
    super(props);
    this.state = { id: 0 };
  }

  change = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  deletefun = (e) => {
    e.preventDefault();
    console.log(this.state);
    axios
      .delete(`https://jsonplaceholder.typicode.com/users/${this.state.id}`)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    return (
      <>
        <h1>Delete Request</h1>
        <div>
          id:
          <input
            type="number"
            name="id"
            value={this.state.id}
            onChange={this.change}
          ></input>
        </div>
        <button type="submit" onClick={this.deletefun}>
          Delete
        </button>
      </>
    );
  }
}

export default DeleteMethod;
