import React, { Component } from "react";
import axios from "axios";

class GetMethod extends Component {
  constructor(props) {
    super(props);
    this.state = { data1: [], errors1: "", data2: [], errors2: "" }; //state for storing the data and errors
  }

  //in function component we can use useState and useEffect for this
  //componentDidMount run when page-load so we fetch data here
  componentDidMount() {
    axios
      .get("https://reqres.in/api/users/") //with reqres
      .then((response) => {
        this.setState({ data1: response.data.data });
        console.log(this.state.data1);
      })
      .catch((error) => {
        this.setState({ errors1: "Error!" });
        console.log(error);
      });

    axios
      .get("https://jsonplaceholder.typicode.com/users") //with jsonplaceholder
      .then((response) => {
        this.setState({ data2: response.data });
        console.log(this.state.data2);
      })
      .catch((error) => {
        this.setState({ errors2: "Error!!!" });
        console.log(error);
      });
  }

  render() {
    const { data1, errors1, data2, errors2 } = this.state;
    return (
      <>
        <h1>Get Request</h1>
        <h3>list of iteams with reqres data</h3>
        <div className="item-div">
          {/* display with map function in array */}
          {data1.map((data) => {
            return (
              <div key={data.id}>
                <p>
                  <strong>{data.first_name}</strong>
                </p>
                <p>{data.email}</p>
                <img src={data.avatar} alt={data.last_name} />
              </div>
            );
          })}
          {errors1 && <div>{errors1}</div>}
        </div>

        <h3>list of iteams with jsonplaceholder data</h3>
        <div className="item-div">
          {data2.map((data) => {
            return (
              <div key={data.id}>
                <p>
                  <strong>{data.name}</strong>
                </p>
                <p>{data.email}</p>
                <p>{data.phone}</p>
                <p>
                  <a href={data.website} target="_blank">
                    Website
                  </a>
                </p>
              </div>
            );
          })}
          {errors2 && <div>{errors2}</div>}
        </div>
      </>
    );
  }
}

export default GetMethod;
