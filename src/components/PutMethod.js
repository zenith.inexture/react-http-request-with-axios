import React, { Component } from "react";
import axios from "axios";

class PutMethod extends Component {
  constructor(props) {
    super(props);
    this.state = { id: 0, name: "", username: "", email: "" };
  }

  change = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  //we can post data from submit button
  putfun = (e) => {
    e.preventDefault();
    console.log(this.state);
    axios
      .put(
        `https://jsonplaceholder.typicode.com/users/${this.state.id}`,
        this.state
      )
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    return (
      <>
        <h1>Put Request</h1>
        <div>
          id:
          <input
            type="number"
            name="id"
            value={this.state.id}
            onChange={this.change}
          ></input>
        </div>
        <div>
          First Name:
          <input
            type="text"
            name="name"
            value={this.state.name}
            onChange={this.change}
          ></input>
        </div>
        <div>
          Last Name:
          <input
            type="text"
            name="username"
            value={this.state.username}
            onChange={this.change}
          ></input>
        </div>
        <div>
          Email:
          <input
            type="email"
            name="email"
            value={this.state.email}
            onChange={this.change}
          ></input>
        </div>
        <button type="submit" onClick={this.putfun}>
          Change
        </button>
      </>
    );
  }
}

export default PutMethod;
