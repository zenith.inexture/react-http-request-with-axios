import axios from "axios";
import React, { Component } from "react";

class PostMethod extends Component {
  constructor(props) {
    super(props);
    this.state = { name: "", username: "", email: "" };
  }

  //we can use formik or react-hook-form library also for onchange and validation in form
  change = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  //we can post data from submit button
  submitfun = (e) => {
    e.preventDefault();
    console.log(this.state);
    //if we work in database then we can directly see the change
    axios
      .post("https://jsonplaceholder.typicode.com/users", this.state)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    return (
      <>
        <h1>Post Request</h1>
        <div>
          First Name:
          <input
            type="text"
            name="name"
            value={this.state.name}
            onChange={this.change}
          ></input>
        </div>
        <div>
          Last Name:
          <input
            type="text"
            name="username"
            value={this.state.username}
            onChange={this.change}
          ></input>
        </div>
        <div>
          Email:
          <input
            type="email"
            name="email"
            value={this.state.email}
            onChange={this.change}
          ></input>
        </div>
        <button type="submit" onClick={this.submitfun}>
          Submit
        </button>
      </>
    );
  }
}

export default PostMethod;
